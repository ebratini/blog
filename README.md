## Coursera/University of New Mexico
#Web Application Architectures: blog application

This is the blog application for the
[*Coursera's Web Application Architectures MOOC*](https://www.coursera.org/course/webapplications)
by [Greg Heileman](https://www.coursera.org/instructor/gregheileman).